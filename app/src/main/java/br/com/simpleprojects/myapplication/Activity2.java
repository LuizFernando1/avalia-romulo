package br.com.simpleprojects.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

public class Activity2 extends AppCompatActivity {
    private float calcCelsius;
    private float calcKelvin;
    private EditText ValorCelso;
    private EditText ValorKelvin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);
    }

    public float getCalcCelsius() {

        ValorCelso= findViewById(R.id.ValorCelso);
        String valor=ValorCelso.getText().toString();
        return calcCelsius;
    }


    public float getCalcKelvin(){

        ValorKelvin= findViewById(R.id.ValorKelvin);
        String valor=ValorKelvin.getText().toString();
        return calcKelvin;
    }
}
