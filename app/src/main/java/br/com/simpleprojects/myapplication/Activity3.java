package br.com.simpleprojects.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

public class Activity3 extends AppCompatActivity {
    private float calcFireneit;
    private float calcKelvin;
    private EditText valorFireneit;
    private EditText valorKelvin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_3);
    }

    public float getCalcFireneit() {

        valorFireneit = findViewById(R.id.valorFinereit);
        String valor = valorFireneit.getText().toString();
        return calcFireneit;
    }

    public float getCalcKelvin(){

        valorKelvin = findViewById(R.id.valorKelvin);
        String valor = valorKelvin.getText().toString();
        return calcKelvin;
    }
}
